# Integration
This file describes the steps nessecary to deploy a flow in the system by using this template project.

## Fork from master<br/>
First of the project has to be forked. By doing this a copy of this project will be created in another environment.<br/>
**_Note:_** Only the project with its files will be forked not specific settings. The CICD variables of the project will not be transferred.

A fork can be created by using the fork button:
<div align="center"><img src="img/Integration_Fork1.PNG"/></div>
Afterwards select the personal namespace and let the system prepare the fork.
<div align="center"><img src="img/Integration_Fork2.PNG"/></div>

## Rename Project<br/>
Since the project will not reside within the personal namespace a unique Project name has to be chosen.

## Remove Fork<br/>
## Rename Project’s Path<br/>
## Transfer Project to fiware group<br/>
## Enable kubernetes Runner for project<br/>
## Change deployment target to adjusted names (deployment.yaml)<br/>
## Change deployment target to adjusted names (service.yaml)<br/>
## Change deployment target to adjusted names (gitlab-ci.yaml)<br/>
## Setup CICD Variables<br/>
## Remove content of img folder<br/>
## Update Readme for current project, refer to template project, add flow descriptions<br/>
## Remove this Integration.md file<br/>
