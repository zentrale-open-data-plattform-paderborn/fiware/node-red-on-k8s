# Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh
# This file is covered by the EUPL license.
# You may obtain a copy of the licence at
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

FROM nodered/node-red

# package.json will be mounted during CI Process

RUN npm install --unsafe-perm --no-update-notifier --no-fund --only=production

# Install additional nodes here
# i.e:
# RUN npm install node-red-node-email


# NOTE: Copying settings.js, flow.json is not nessecary since they will be
#       mounted and therefore replaced to /data during the CI Process.