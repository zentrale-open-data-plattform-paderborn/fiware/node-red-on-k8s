# Node-Red on k8s
This project reflects the general approach on how to deploy a Node-RED environment on kubernetes by using the version control and CI/CD systems of GitLab.

In addition to that, this project is set up to be a template for future flow deployments. To use this template please refer to the [Integration.md Document](Integration.md) 
(This document will be completed during the first [API integration](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/nodered-webecho-demo/).)

## Deployment
This section describes the deployment process for this project. The following picture gives a brief overview, what the goal of the CI/CD process is. Afterwards it is 
described in more detail, how this goal is achieved by showcasing the involved stages and involved files.
<div align="center"><img src="img/NodeRED_GitLab.PNG"/></div>

*(Where ` {stage} ` is the corresponding domain environment.)* <br/>
The deployment process of the Node-RED environment is implemented using the GitLab CI/CD mechanism. The CI/CD definition can be found in [gitlab-ci.yml](.gitlab-ci.yml) file. 
The implemented `build`, `deploy` and a `delete` stage are described in the following sections. 

### Build stage
During the `build` stage, a docker image for the Node-RED environment is built. The official [nodered/node-red](https://hub.docker.com/r/nodered/node-red/) image from DockerHub is 
used as a basic image. The additional information about the Node-RED docker image can be found in the [documentation](https://nodered.org/docs/getting-started/docker). 
For the building process we need the following files in the project directory:
* `Dockerfile`: Contains definitions to create the docker image and package installations. [The file's origin](https://nodered.org/docs/getting-started/docker)
Taken from official documentation. Adjusted and documented accordingly to use file mounting.

At the end of the build job the new image is created and pushed into the internal GitLab repository.

### Deploy stage
For the `deploy` stage we use the image from the previous build stage to deploy the Node-RED Application on kubernetes. 
Depending on the branch name we can deploy the application in the `fiware-dev`-, `fiware-staging`- or `fiware-prod`-environment. 
The CI/CD Variables `KUBECONFIG` and `KUBECONFIG_PROD` hidden in the project settings are used by the kubectl client tool to access the specific namespace on the cluster.<br/>
During the deploy process we create a kubernetes secret from the hidden CI/CD Varibale `REGISTRY_ACCESS_TOKEN`,
this secret is needed to pull the image from the internal GitLab repository.<br/> 
In order to configure the Node-Red application the following files will be mounted to the deployment:
* `package.json`: This file will contain dependencies of the image setup during the build process. The 
[original file](https://github.com/node-red/node-red-docker/blob/master/package.json) is taken from node-red-docker project.
* `flows/first.json`: Defines the flow, to be run after deployment. For this project a simple flow outputting the current time to debug and pod log is implemented.
* `settings.js`: The configuration file for the node-red application. The 
[original file](https://github.com/node-red/node-red/blob/master/packages/node_modules/node-red/settings.js) is take from node-red-docker projekt and only
the option for adding the admin account (see Appendix A) has been added. A guide on possible options to adjust the settings can be found in the 
[official documentation](https://nodered.org/docs/user-guide/runtime/settings-file)

For the deployment itself, the following files (kubernetes manifests) are required for running the Node-RED on the cluster:

* `deployment.yaml`: Defines the deployed image, the pod's given name and the files issued to be mounted (listed above).
* `service.yaml`: Configures the domain name and port inside the cluster in order to make the application accessible.

For the deployment and file-mounts the following container definition is used:

  ```yaml
      spec:
      containers:
      - name: nodered
        image: nodered/node-red
        volumeMounts:
          - name: settings
            mountPath: /data/settings.js
            subPath: settings.js
          - name: package
            mountPath: /usr/src/node-red/package.json
            subPath: package.json
          - name: flows
            mountPath: /data/flows.json
            subPath: flows.json
      volumes:
        - name: settings
          configMap:
            name: nodered
            items:
            - key: settings.js
              path: settings.js
        - name: package
          configMap:
            name: nodered
            items:
            - key: package.json
              path: package.json
        - name: flows
          configMap:
            name: nodered
            items:
            - key: first.json
              path: flows.json
  ```
As stated above the files settings.js, package.json and flows/first.json will be mounted in Pod Volumes over a ConfigMap.
This ConfigMap is first created during the CI-Process, seen in the [gitlab-ci.yml](.gitlab-ci.yml) file:

```yaml
  kubectl create configmap nodered \
    --from-file=settings.js \
    --from-file=package.json \
    --from-file=flows/first.json \
    -n ${NAMESPACE} --kubeconfig "$(pwd)/kube.config" --dry-run=client -o yaml | kubectl apply -f -
```

* `service.yaml`: defines the domain name and port inside the cluster. The service name was modified to `nodered`.

At the end of the deploy job the Node-RED is deployed and started on the cluster.

### Delete stage
In order to be able to stop/delete a currently running flow, the manual `delete` job is added as a stage to the
CI Process. Once the job has been triggered, the current `nodered` Deployment and the `nodered` Service (where 
`nodered` is the specified name from `deployment.yaml` and `service.yaml`) definitions will be deleted from cluster.<br/>
Depending on the branch name we can delete the application in the `fiware-dev`-, `fiware-staging`- or `fiware-prod`-environment.<br/>
Finally at the end of the delete job the active Pod containing the Node-Red application is terminated.

## Access Node-RED
There are two option on accessing the NodeRED environment, after project has deployed the image. 

### Configure a website backend within the umbrella proxy.
The website backend has to be configured once for the corresponding kubernetes pod via the Umbrella UI.<br/>
The exemplary setting would look like this:<br/>
<div align="center"><img src="img/NodeRED_Umbrella.PNG"/></div>

After this configuration the following image describes where the access is located in the system.
<div align="center"><img src="img/NodeRED_ProxyAccess.PNG"/></div>

*(Where ` {stage} ` is the corresponding domain environment.)* <br/>

### Establish a connection via tunnel
The second option is to tell kubernets to forward a port (specified in the settings.js file) of the pod for the user.<br/>
For this to work you will need to have the kubernetes config file to access the cluster and the command line tool [`kubectl` installed](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/).<br/>
Establishing a tunnel to foward the port used by the Node-RED deployment the `kubectl` command can be used in the following way:<br/>
```
kubectl port-forward service/[SERVICE_NAME] 1880:1880 -n fiware-dev --kubeconfig=[KUBERNETES_KONFIG_FILE]
```
The result should look similar:
<div align="center"><img src="img/NodeRED_Tunnel.PNG"/></div>

**Note 1:** This port will stay open until the connection is closed by the user either by closing the terminal window or pressing `CTRL+C`<br/>
**Note 2:** After establishing a tunnel, the web frontend is available at `127.0.0.1:1880`

After this configuration the following image describes where the access is located in the system.
<div align="center"><img src="img/NodeRED_TunnelAccess.PNG"/></div>

*(Where ` {stage} ` is the corresponding domain environment.)* <br/>

### Accessing the Web UI

Either way, since the flows may contain sensitive data like at least an api key, the user is presented a login screen:<br/>
<div align="center"><img src="img/NodeRED_Login.PNG"/></div>

After a successful login, the web frontend of Node-RED should look similar to this.<br/>
<div align="center"><img src="img/NodeRED_UI.PNG"/></div>

If the user wants to access the current log, the `kubectl` command can be used as well:<br/>
`kubectl -n fiware-dev --kubeconfig=config.yaml logs PODNAME`, where PODNAME can be retrieved by using `get pods`.

**_Restriction Note 1:_** Using the web frontend without establishing a tunnel will disconnect after login. This results in the flow being visible, but logs will not be shown, since sensitive data may be logged here.<br/>
**_Restriction Note 2:_** Even though the admin user is able to change the flow, it is not possible to deploy changes. If attempted, the user is prompted with an (maybe misleading) error message.

## Web UI Restrictions
Due to the fact, that for Node-RED is missing the features version control and masking of secrets, we have to deal with this problems.

### Masking of secrets: Login mechanics
Since the deployed flows need some secrets to interact with i.e. APIs (api keys), the flows visible in the Web UI must not be public. This is achivied by extending the settings.js with the adminAuth parameter, which enables the login mechanic shown in the previous section:<br/>
This parameter contains the username, the password hash string (see Appendix A for more information) and the restriction level for that account.<br/>
Once this account is set, the flows can only be viewed after user successfully logged in.

### Version Control: Read-Only
Since the Web UI shall be exposed, any changes done by using the Web UI are not reflected in GitLab, so changes would go by unnoticed and would not be embedded in the set up CI/CD processes.<br/>
To avoid that, the admin account (see previous section) will be given only read access. So changes cannot be done without a commit to the repository.

## Appendix A - Admin Account Shadowpassword

The password field of the `adminAuth` section in the [settings.js](settings.js) file is a String representing a shadowpassword created by the [bcrypt hash function](https://en.wikipedia.org/wiki/Bcrypt).<br/>
This String consists of fields sepreated by the `$` sign.

### The $ sign in GitLab
The `$` has a special meaning when it comes to the GitLab environment. But more important, we would like to use the hash String in a CI_Variable.

But here emerge two problems:
1. CI_Variables can only be masked in the CI process, if they do not contain certain special characters like `$`. Therefore for any developer it is possible to (if intended or not) make the content of the CI_Variable visible during the CI process and disclose in the pipeline of the project.
2. If used as a CI_Varialbe the special meaning of `$` comes with the obligation to escape the character. Which has to be remembered during creation or change of the password.

### Possible implementations

1. **CI_Variable**   
Even with the problems, the hash string can go into the CI_Variable

2. **Multiple CI_Variables (currently implemented)**  
To avoid the escaping of characters it would be possible to use multiple CI_Variables and construct the hash string during the CI process, by inserting the `$` during the build step.<br/>
This solution results in more effort then the previous one for password creation and changing, since several manual steps are involved. But it yields the most secure solution since it would enable the option to mask the hash String during CI process.

3. **Plain**  
The second option is to leave the shadowpassword as it is in the settings.js file.  
Obviously, since the file is public, everyone can see it. But due to the chosen hash function it is not possible to disassemble the password from the hash with a reasonable amount of effort.

## Appendix B - Changing the password
Since the current approach requires several manual steps, this appendix describes how to change a password.<br/>
You will need a machine with Node-RED installed.
1. Connect to a machine with the node-red command available.
2. Visit [NR User-Doc](https://nodered.org/docs/user-guide/runtime/securing-node-red#generating-the-password-hash) and generate a hash.
This should give you something like this: `admin:$2y$08$bfpwQs6edKaCAGOA8ZQpEOvhXHECZEuUXeoaUH7CinLWch1HAhfma`  

**Note:** An error may emerge during the `node` command: _Cannot find module 'bcryptjs'_. If this is the case the missing package needs to be installed: `npm install bcryptjs`

3. Split the String  
Header:  `$2y$08$`  
Payload: `bfpwQs6edKaCAGOA8ZQpEOvhXHECZEuUXeoaUH7CinLWch1HAhfma`  
4. Escape the header, by adding `$` to every present `$`   
Header:  `$2y$08$` -> `$$2y$$08$$`  
5. In Gitlab: Go to Settings -> CI / CD -> Expand Variables.  
6. Change `ADMIN_USER_PWD_HASH_HEADER` value to `$$2y$$08$$`  
7. Change `ADMIN_USER_PWD_HASH_PAYLOAD` value to `bfpwQs6edKaCAGOA8ZQpEOvhXHECZEuUXeoaUH7CinLWch1HAhfma`  
8. With the next environment deploy, the new password can be used to access the web frontend.
9. Check your history `history`.
10. Remove the history entry containing the password `history -d NUMBER`.



**Remark** It is possible to use MD5 as function for bcrypt, but this is not advised.

**Remark** For each implementation the chosen password should match an appropriate complexity:<br/>
[How to choose a password](https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/Passwoerter/passwoerter_node.html)<br/>
[How to handle a password](https://www.bsi-fuer-buerger.de/BSIFB/DE/Empfehlungen/Passwoerter/Umgang/umgang.html)

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
